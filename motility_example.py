import torch
from packages.model import LatentCRN
import logging
from utils.figure_configuration_neurips import figure_configuration_neurips
from utils.utils import set_rng_seed

set_rng_seed(1)

figure_configuration_neurips(k_width_height=1.5, columnwidth_cm=4.67)
logging.basicConfig(level=logging.INFO)

# Motility example (https://arxiv.org/pdf/1409.4362.pdf)
num_species = 9
num_reactions = 12

subs_indicies = [[0], [1], [2], [3], [4], [6], [3, 5], [4], [1, 2], [7], [1, 5], [8]]
prod_indicies = [[0, 1], [], [2, 3], [], [3, 5, 6], [], [4], [3, 5], [7], [1, 2], [8], [1, 5]]
subs_stoch = torch.zeros(num_species, num_reactions, dtype=torch.int)
prod_stoch = torch.zeros(num_species, num_reactions, dtype=torch.int)
for r in range(num_reactions):
    subs_stoch[subs_indicies[r], r] = 1
    prod_stoch[prod_indicies[r], r] = 1

rates = torch.tensor([0.1, 0.0002, 1., 0.0002, 1.0, 0.0002, 0.01, 0.1, 0.02, 0.1, 0.01, 0.1])
obs_mat = torch.zeros(1, num_species)
obs_mat[0, 3] = 1
obs_cov = 4.0 * torch.eye(1)
initial_counts = torch.tensor([1, 10, 1, 10, 1, 1, 10, 1, 1])
init_dist_param = initial_counts.log()

crn_model = LatentCRN(subs_stoch, prod_stoch, rates, obs_mat, obs_cov, init_dist_param)
t_span = torch.tensor([0., 100.])
obs_times = torch.linspace(0.1, 99.9, 50)

latent_trajectory, obs_times, obs_counts = crn_model.simulate(initial_counts, t_span, obs_times)

rates_initial = torch.tensor([0.1, 0.0002, 0.5, 0.0002, 1.0, 0.0002, 0.01, 0.1, 0.5, 0.5, 0.01, 0.1])
optimize_rates = torch.zeros_like(rates_initial, dtype=torch.bool)
optimize_rates[[2, 8, 9]] = True
initial_params = dict(rates=rates_initial,
                      optimize_rates=optimize_rates)

nat_params_smooth, nat_params_filt, params = crn_model.fit_params(t_span, obs_times, obs_counts, initial_params)

#Visualize results
import matplotlib.pyplot as plt

names = ['codY', 'CodY', 'flache', 'SigD', 'SigD_hag', 'hag', 'Hag', 'CodY_flache', 'CodY_hag']
t_grid = torch.linspace(*t_span, 1000)
nat_params_smoother_grid = nat_params_smooth(t_grid)
for i, (latent_trajectory_count, nat_param_smoother_grid) in enumerate(
        zip(latent_trajectory.counts.T, nat_params_smoother_grid)):
    plt.figure()
    handle_gt, = plt.step(latent_trajectory.times, latent_trajectory_count, linestyle='dashed')
    handle_mean, = plt.plot(t_grid, nat_param_smoother_grid.exp(), color=handle_gt.get_color())
    if i == 3:
        plt.scatter(obs_times, obs_counts[:, 0], color=handle_gt.get_color(), marker='x')
    plt.xlabel('Time $t$')
    plt.ylabel('{}'.format(names[i]))  # $X_{}(t)$ ,i+1
    plt.tight_layout(pad=0)
    plt.savefig('motility_{}_{}'.format(names[i], i) + '.pdf', format='pdf')
    plt.show()