import torch
from packages.model import LatentCRN
import logging
from utils.figure_configuration_neurips import figure_configuration_neurips
from utils.utils import set_rng_seed

set_rng_seed(2)

figure_configuration_neurips()
logging.basicConfig(level=logging.INFO)

# Predator Prey example
subs_stoch = torch.tensor([[1, 1, 0], [0, 1, 1]])
prod_stoch = torch.tensor([[2, 0, 0], [0, 2, 0]])
rates = torch.tensor([5e-4, 1e-4, 5e-4])
obs_mat = torch.eye(2)
obs_cov = 2.0 * torch.eye(2)
initial_counts = torch.tensor([19, 7])
init_dist_param = initial_counts.log()

crn_model = LatentCRN(subs_stoch, prod_stoch, rates, obs_mat, obs_cov, init_dist_param)
t_span = torch.tensor([0., 3000.])
obs_times, _ = torch.sort(t_span[0] + torch.rand(50) * t_span[-1])
latent_trajectory, obs_times, obs_counts = crn_model.simulate(initial_counts, t_span, obs_times)

initial_params=dict() #no parameter learning

nat_params_smooth, nat_params_filt, params=crn_model.fit_params(t_span,obs_times,obs_counts,initial_params)

#Visualize results
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')

plt.figure()
t_grid=torch.linspace(*t_span,1000)
nat_params_smoother_grid=nat_params_smooth(t_grid)
phase=torch.meshgrid(t_grid,torch.arange(30).to(dtype=torch.float))
log_score=[]
for t, nat_params_smoother, count in zip(t_grid, nat_params_smoother_grid.T, phase[1]):
    log_score_t=torch.distributions.Poisson(nat_params_smoother.exp()).log_prob(count[...,None])
    log_score.append(log_score_t)
log_score = torch.stack(log_score)

c_light = matplotlib.colors.colorConverter.to_rgba('white', alpha = 0)
handles=[]
colors=['#1f77b4','#ff7f0e']
c_dark= matplotlib.colors.colorConverter.to_rgba(colors[0], alpha = 1)
cmap_rb = matplotlib.colors.LinearSegmentedColormap.from_list('rb_cmap', [c_light, c_dark], 512)
plt.pcolor(*phase,  log_score.T[0].exp().T, antialiaseds=True, rasterized=True,cmap=cmap_rb,norm=matplotlib.colors.Normalize(vmax=.15))
c_dark= matplotlib.colors.colorConverter.to_rgba(colors[1], alpha = 1)
cmap_rb = matplotlib.colors.LinearSegmentedColormap.from_list('rb_cmap', [c_light, c_dark], 512)
plt.pcolor(*phase, log_score.T[1].exp().T, antialiaseds=True, rasterized=True,cmap=cmap_rb,norm=matplotlib.colors.Normalize(vmin=.01))

for i, (latent_trajectory_count, nat_param_smoother_grid, obs_count) in enumerate(zip(latent_trajectory.counts.T, nat_params_smoother_grid, obs_counts.T)):
    c_dark= matplotlib.colors.colorConverter.to_rgba(colors[i], alpha = .5)
    cmap_rb = matplotlib.colors.LinearSegmentedColormap.from_list('rb_cmap', [c_light, c_dark], 512)
    handle_gt, = plt.step(latent_trajectory.times, latent_trajectory_count, linestyle='dashed')
    handle_mean, =plt.plot(t_grid, nat_param_smoother_grid.exp(), color=colors[i])
    # plt.plot(t_grid, nat_param_smoother_grid.exp().floor(), color=handle_gt.get_color(), linestyle='dotted') #Posterior MAP
    plt.scatter(obs_times, obs_count, color=colors[i], marker='x')
    handles.append(handle_mean)

plt.xlabel('Time $t$')
plt.ylabel('Abundance $X_i(t)$')
plt.tight_layout(pad=0)
plt.savefig('lv' + '.pdf', format='pdf')
plt.show()

handles[0].set_label('Prey $\mathsf X_1$')
handles[1].set_label('Predator $\mathsf X_2$')
for i, h in enumerate(handles):
    plt.figure()
    plt.figlegend(handles=[h], frameon=False)
    plt.savefig('lv_' + 'legend_' + str(i) + '.pdf', format='pdf')
    plt.show()

pass