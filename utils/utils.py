import torch
import numpy as np
import random

def set_rng_seed(rng_seed):
    """
    Sets seeds for torch, random, np

    :param rng_seed: seed value
    """
    torch.manual_seed(rng_seed)
    random.seed(rng_seed)
    np.random.seed(rng_seed)