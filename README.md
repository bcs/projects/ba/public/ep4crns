# Introduction
This is the companion code to the preprint 'Entropic Matching for Expectation Propagation of
Markov Jump Processes' [B. Alt and H. Koeppl].

Simulations similar to the ones used in the paper can be run with this python code.
