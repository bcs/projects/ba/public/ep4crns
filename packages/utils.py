import torch
from torch.special import gammaln
from scipy.special import factorial as sp_factorial
from scipy.interpolate import interp1d as interp1d_sp
import numpy as np
from scipy.integrate import solve_ivp as solve_ivp_sp
from scipy.integrate._ivp.common import OdeSolution as OdeSolutionSP

EPS=1e-10
def falling_factorial(x, m):
    return torch.tensor(falling_factorial_np(x.numpy(), m.numpy()), dtype=x.dtype, device=x.device)


@np.vectorize
def falling_factorial_np(x, m):
    return np.prod(x - np.arange(m))


def factorial(x):
    # noinspection PyUnresolvedReferences
    return sp_factorial(x).to(dtype=x.dtype)


class interp1d(interp1d_sp):
    def __init__(self, x, y, **kwargs):
        self.torch_dtype = y.dtype
        self.torch_device = y.device
        super().__init__(x, y, **kwargs)

    def __call__(self, x):
        return torch.tensor(super().__call__(x), dtype=self.torch_dtype, device=self.torch_device)


class OdeSolution(OdeSolutionSP):
    def __init__(self, ts, interpolants, device=None, dtype=None):
        self.device = device
        self.dtype = dtype
        super().__init__(ts, interpolants)

    def __call__(self, t):
        return torch.as_tensor(super().__call__(torch.as_tensor(t).detach().numpy()), dtype=self.dtype,
                               device=self.device)


def solve_ivp_dense(fun, t_span, y0, method='RK45', t_eval=None,
                    events=None, vectorized=False, args=None, **options):
    numpy_fun = lambda t, y: fun(float(t), torch.from_numpy(y)).detach().numpy()
    res = solve_ivp_sp(numpy_fun, t_span, y0.detach().numpy(), method=method, t_eval=t_eval, dense_output=True,
                       events=events, vectorized=vectorized, args=args, **options)
    return OdeSolution(res.sol.ts, res.sol.interpolants, device=y0.device, dtype=y0.dtype)


def concat_ode_solution(list_of_ode_solution):

    ts_list=[sol.ts[:-1] for sol in list_of_ode_solution]
    ts=np.concatenate((*ts_list, np.array([list_of_ode_solution[-1].ts[-1]])))
    interpolants = [interpolants for sol in list_of_ode_solution for interpolants in sol.interpolants]
    device = list_of_ode_solution[-1].device
    dtype = list_of_ode_solution[-1].dtype

    return OdeSolution(ts, interpolants, dtype=dtype, device=device)