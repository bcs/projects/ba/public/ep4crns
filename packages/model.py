import torch
from packages.utils import factorial, falling_factorial, interp1d, solve_ivp_dense, OdeSolution, concat_ode_solution
from collections import namedtuple
from torch.linalg import cholesky, solve
from torch.distributions.categorical import Categorical
from torch.distributions.exponential import Exponential
from torch.distributions.multivariate_normal import MultivariateNormal
import logging

DiscreteEvent = namedtuple('DiscreteEvent', ['time', 'reaction_idx', 'count'])

EPS_lambda = 1e-6  # Minimum parameter value


class LatentCRN:
    def __init__(self, subs_stoch, prod_stoch, rates, obs_mat, obs_cov, init_dist_param):
        """
        Model for a latent chemical reaction network (CRN)

        :param subs_stoch: [num_species x num_reactions] substrate stoichiometry
        :param prod_stoch: [num_species x num_reactions] product stoichiometry
        :param rates: [num_reactions] reaction rate parameters
        :param obs_mat: [obs_dim x num_species] linear observation matrix H, such that y= H x+ epsilon
        :param obs_cov: [obs_dim x obs_dim] observation covariance, i.e., E[epsilon epsilon^\top]
        :param init_dist_param: [num_species] log of mean of the initial distribution per species log(E[X(0)])
        """

        # Store inputs
        self.num_species, self.num_reactions = subs_stoch.shape
        self.sub_stoch = subs_stoch
        self.prod_stoch = prod_stoch
        self.stoch = prod_stoch - subs_stoch
        self.rates = rates
        self.obs_mat = obs_mat
        self.obs_cov = obs_cov
        self.init_dist_param = init_dist_param

    # rates and effective rates setter and getter
    @property
    def rates(self):
        return self._rates

    @rates.setter
    def rates(self, x):
        self._rates = x
        self._effective_rates = x / torch.prod(factorial(self.sub_stoch), dim=0)

    @property
    def effective_rates(self):
        return self._effective_rates

    @effective_rates.setter
    def effective_rates(self, x):
        self._effective_rates = x
        self._rates = x * torch.prod(factorial(self.sub_stoch), dim=0)

    # observation model parameter setter and getter
    @property
    def obs_cov(self):
        return self._obs_cov

    @obs_cov.setter
    def obs_cov(self, x):
        self._obs_cov = x
        self.obs_cho = cholesky(x)

    def hazards(self, counts):
        """
        computes the hazards (propensities) using mass action kinetics

        :param counts: [num_species] number of each species
        :return: [num_reactions] computed hazard
        """
        return self.effective_rates * torch.prod(falling_factorial(counts[..., None], self.sub_stoch), dim=-2)

    def simulate(self, initial_counts, t_span, obs_times, max_events=int(1e6)):
        """
        Simulates the latent CRN with observations

        :param initial_counts: [num_species] initial number of species at start of simulation
        :param t_span: vector of start and end point
        :param obs_times: times of observations
        :param max_events: Optional parameter for simulation
        :return: latent_trajectory, obs_times, obs_counts
        """
        logging.info('Latent state simulation')
        list_of_events = self._simulate_latent(initial_counts, t_span, max_events=max_events)
        latent_trajectory = CRNTrajectory(list_of_events)
        logging.info('Observations simulation')
        obs_counts = self._simulate_obs(latent_trajectory, obs_times)
        return latent_trajectory, obs_times, obs_counts

    def _simulate_latent(self, initial_counts, t_span, max_events=int(1e6)):
        """
        Simulates the latent CRN process using the stochastic simulation algorithm (SSA)

        :param initial_counts:  [num_species] initial number of species at start of simulation
        :param t_span: vector of start and end point
        :param max_events: Optional parameter for simulation
        :return: list_of_events for the latent process
        """

        t0, T = t_span
        t = t0.clone()
        counts = initial_counts.clone()

        list_of_events = [DiscreteEvent(t.clone(), None, counts.clone())]  # Store initial
        # SSA
        for _ in range(max_events):
            hazards = self.hazards(counts)
            total_hazard = hazards.sum()

            # Sample waiting time
            t += Exponential(rate=float(total_hazard)).sample()
            if t > T:
                break

            # Sample reaction
            reaction_idx = Categorical(probs=hazards / total_hazard).sample()

            # Update state
            counts += self.stoch[..., reaction_idx]

            # Store Event
            list_of_events.append(DiscreteEvent(t.clone(), reaction_idx.clone(), counts.clone()))

        list_of_events.append(DiscreteEvent(T.clone(), None, counts.clone()))  # Store final
        return list_of_events

    def _simulate_obs(self, latent_trajectory, obs_times):
        """
        Simulates the observations using the latent trajectory and the observation times

        :param latent_trajectory: (callback function: input time output state) latent state trajectory
        :param obs_times: [num_obs] observation times, where the latent process is measured
        :return: [num_obs x obs_dim] observation_counts for the observation times
        """
        counts = latent_trajectory(obs_times)
        assert counts.ndim == 2
        return MultivariateNormal(
                loc=(self.obs_mat[None] @ counts[..., None].to(dtype=self.obs_mat.dtype)).squeeze(dim=-1),
                scale_tril=self.obs_cho[None]).sample()

    def filter_drift(self, nat_param):
        """
        Drift function for the entropic matching filter

        :param nat_param: [num_species] natural parameter of the variational filtering distribution
        :return: [num_species] rate of change in the natural filtering parameters
        """
        return ((-nat_param[..., None]).exp() * self.effective_rates * self.stoch * (
            (self.sub_stoch * nat_param[..., None]).sum(axis=-2)).exp()).sum(axis=-1)

    def smoother_drift(self, nat_param, nat_param_filter):
        """
        Drift function for the entropic matching backward smoother

        :param nat_param: [num_species] natural parameter of the variational smoothing distribution
        :param nat_param_filter: [num_species] natural parameter of the variational filtering distribution
        :return: [num_species] rate of change in the natural smoothing parameters
        """
        return ((-nat_param[..., None]).exp() * self.effective_rates * self.stoch * (
            (self.sub_stoch * nat_param[..., None] + self.stoch * (nat_param - nat_param_filter)[..., None]).sum(
                    axis=-2)).exp()).sum(axis=-1)

    def compute_sites(self, obs_counts, cav_param):
        """
        Performs the site update of the expectation propagation method

        :param obs_counts: [num_obs x obs_dim] given observations
        :param cav_param: [num_obs x num_species] cavity parameter (natural parameter of the variational smoothing distribution)
        :return: [num_obs x num_species] site parameters (likelihood contribution in natural parameter space)
        """
        F = torch.diag_embed((cav_param).exp())
        innovation = obs_counts[..., None] - self.obs_mat @ cav_param.exp()[..., None]
        return (cav_param.exp() + (
                F @ self.obs_mat.T @ solve(self.obs_mat @ F @ self.obs_mat.T + self.obs_cov, innovation)).squeeze(
                dim=-1)).clip(min=EPS_lambda).log() - cav_param

    def infer_counts(self, t_span, obs_times, obs_counts, options=None):
        """
        Performs expectation propagation for latent state inference

        :param t_span: vector of start and end point
        :param obs_times:  [num_obs] observation times, where the latent process is measured
        :param obs_counts: [num_obs x obs_dim] observation_counts for the observation times
        :param options: dictionary containing:
                        max_iter_ep: number of maximum iterations (default: 100)
                        learning_rate_ep: learning rate used for damped message passing (default: 5e-2)
                        abs_tol_ep: convergence threshold (default: 1e-2)

        :return: nat_params_smooth, nat_params_filt (callback functions)
        """

        # Get options
        if options is None:
            options = dict()
        try:
            max_iter_ep = options['max_iter_ep']
        except KeyError:
            options['max_iter_ep'] = 100
            max_iter_ep = options['max_iter_ep']
        try:
            learning_rate_ep = options['learning_rate_ep']
        except KeyError:
            options['learning_rate_ep'] = 5e-2
            learning_rate_ep = options['learning_rate_ep']
        try:
            abs_tol_ep = options['abs_tol_ep']
        except KeyError:
            options['abs_tol_ep'] = 1e-2
            abs_tol_ep = options['abs_tol_ep']

        # Initialization
        try:
            sites = options['initial_sites']
        except KeyError:
            sites = torch.zeros(obs_times.numel(), self.num_species, device=obs_counts.device)

        times_full = torch.stack((t_span[0], *obs_times, t_span[-1]))
        nat_params_filt = None
        nat_params_smooth = None

        logging.info('Starting EP algorithm')
        # Iterate until convergence
        for iter in range(max_iter_ep):
            logging.info('EP algorithm iteration {}'.format(iter + 1))

            # Filter
            initial_nat_params = self.init_dist_param
            sol_list = []
            for i, t_span_interval in enumerate(zip(times_full[:-1], times_full[1:])):
                # Integrate filter
                sol_list.append(
                        solve_ivp_dense(lambda t, nat_params: self.filter_drift(nat_params),
                                        torch.stack(t_span_interval),
                                        initial_nat_params))
                initial_nat_params = sol_list[-1](t_span_interval[-1])

                # Do reset
                if i < obs_times.numel():
                    initial_nat_params += sites[i]

            nat_params_filt = concat_ode_solution(sol_list)

            # Smoother
            nat_params_smooth = solve_ivp_dense(
                    lambda t, nat_params: self.smoother_drift(nat_params, nat_params_filt(t)),
                    torch.flip(t_span, dims=(0,)), initial_nat_params)

            # Update sites
            new_sites = self.compute_sites(obs_counts, nat_params_smooth(obs_times).T)
            sites = (1 - learning_rate_ep) * sites + learning_rate_ep * new_sites

            # Check for convergence
            eps_max = (sites - new_sites).abs().max()
            logging.info('EP algorithm maximum absolute change {}'.format(eps_max))
            if eps_max < abs_tol_ep:
                break

        return nat_params_smooth, nat_params_filt, sites

    def fit_params(self, t_span, obs_times, obs_counts, initial_params, options=None):
        """
        Performs approximate EM algorithm for parameter learning and latent state inference with expectation propagation
        :param t_span: vector of start and end point
        :param obs_times: [num_obs] observation times, where the latent process is measured
        :param obs_counts: [num_obs x obs_dim] observation_counts for the observation times
        :param initial_params: dictionary containing possible initial values parameters to optimize:
                        rates:  [num_reactions] reaction rate parameters
                        obs_mat: [obs_dim x num_species] linear observation matrix H, such that y= H x+ epsilon
                        obs_cov: [obs_dim x obs_dim] observation covariance, i.e., E[epsilon epsilon^\top]
                        init_dist_param: [num_species] log of mean of the initial distribution per species log(E[X(0)])
        :param options: dictionary containing:
                        max_iter_em: number of maximum iterations for expectation maximization (EM) (default: 20)
                        abs_tol_em:  convergence threshold for EM (default: 1e-2)
                        max_iter_ep: number of maximum iterations for expectation propagation (EP) (default: 100)
                        learning_rate_ep: learning rate used for damped message passing in EP (default: 5e-2)
                        abs_tol_ep: convergence threshold for EP (default: 1e-2)
        :return: nat_params_smooth, nat_params_filt, params
        """

        # Get options
        if options is None:
            options = dict()
        try:
            max_iter_em = options['max_iter_em']
        except KeyError:
            options['max_iter_em'] = 20
            max_iter_em = options['max_iter_em']
        try:
            abs_tol_em = options['abs_tol_em']
        except KeyError:
            options['abs_tol_em'] = 1e-2
            abs_tol_em = options['abs_tol_em']
        try:
            warm_starts = options['warm_starts']

        except KeyError:
            options['warm_starts'] = True
            warm_starts = options['warm_starts']

        # Set initial parameters and optimization flags
        try:
            self.rates = initial_params['rates']
            try:
                optimize_rates = initial_params['optimize_rates']
            except KeyError:
                optimize_rates = torch.ones_like(self.rates, dtype=torch.bool)
        except KeyError:
            optimize_rates = torch.zeros_like(self.rates, dtype=torch.bool)
        try:
            self.obs_mat = initial_params['obs_mat']
            optimize_obs_mat = True
        except KeyError:
            optimize_obs_mat = False
        try:
            self.obs_cov = initial_params['obs_cov']
            optimize_obs_cov = True
        except KeyError:
            optimize_obs_cov = False
        try:
            self.init_dist_param = initial_params['init_dist_param']
            optimize_init_dist_param = True
        except KeyError:
            optimize_init_dist_param = False

        # Initalization
        nat_params_smooth = None
        nat_params_filt = None
        params = dict()

        # Iterate until convergence
        logging.info('Starting EM algorithm')
        for iter in range(max_iter_em):
            # Do E-step
            logging.info('Starting E-Step iteration {}'.format(iter))
            nat_params_smooth, nat_params_filt, sites = self.infer_counts(t_span, obs_times, obs_counts, options=options)
            if warm_starts:
                options['initial_sites']=sites

            # Do M Step
            logging.info('Starting M-Step iteration {}'.format(iter))
            init_dist_param_old = self.init_dist_param.detach().clone()
            effective_rates_old = self.effective_rates.detach().clone()
            obs_cov_old = self.obs_cov.detach().clone()
            obs_mat_old = self.obs_mat.detach().clone()

            # M-Step intial distribution
            if optimize_init_dist_param:
                self.init_dist_param = nat_params_smooth(t_span[0]).detach().clone()

            # M-Step reaction rates
            if optimize_rates.any():
                # Compute statistics by integrating an ODE
                def filt_smooth_prop_rate(nat_param_smoother, nat_param_filter):
                    return self.effective_rates ** 2 * ((self.sub_stoch * nat_param_smoother[..., None] + self.stoch *
                                                         (nat_param_smoother - nat_param_filter)[..., None]).sum(
                            axis=-2)).exp()

                def smooth_prop_rate(nat_param_smoother):
                    return self.effective_rates * (
                        (self.sub_stoch * nat_param_smoother[..., None]).sum(axis=-2)).exp()

                avg_filt_smooth_prop = solve_ivp_dense(
                        lambda t, y: filt_smooth_prop_rate(nat_params_smooth(t), nat_params_filt(t)), t_span,
                        torch.zeros_like(self.effective_rates))(t_span[-1]) / (t_span[-1] - t_span[0])
                avg_smooth_prop = solve_ivp_dense(
                        lambda t, y: smooth_prop_rate(nat_params_smooth(t)), t_span,
                        torch.zeros_like(self.effective_rates))(t_span[-1]) / (t_span[-1] - t_span[0])

                # Compute M-Step
                effective_rates_new = self.effective_rates
                effective_rates_new[optimize_rates] = (avg_filt_smooth_prop / avg_smooth_prop)[optimize_rates]
                self.effective_rates = effective_rates_new

            # M-Step observation model parameters
            if optimize_obs_cov or optimize_obs_mat:

                # Compute statistics
                smooth_rates_obs = nat_params_smooth(obs_times).T.exp()
                avg_cross_cor = (obs_counts[..., None] @ smooth_rates_obs[..., None, :]).mean(dim=0)
                avg_smooth_cor = (
                        torch.diag_embed(smooth_rates_obs) + smooth_rates_obs[..., None] @ smooth_rates_obs[...,
                                                                                           None, :]).mean(dim=0)
                # M-Step observation model covariance
                if optimize_obs_cov:
                    # Compute statistic
                    avg_obs_cor = (obs_counts[..., None] @ obs_counts[..., None, :]).mean(dim=0)
                    # Compute M-Step
                    self.obs_cov = avg_obs_cor - self.obs_mat @ avg_cross_cor.T - avg_cross_cor @ self.obs_mat.T + self.obs_mat @ avg_smooth_cor @ self.obs_mat.T

                # M-Step observation model matrix
                if optimize_obs_mat:
                    # Compute M-Step
                    self.obs_mat = solve(avg_smooth_cor, avg_cross_cor, left=False)

            # Compute maximum change in all parameters
            eps_max = torch.stack(((init_dist_param_old - self.init_dist_param).abs().max(),
                                   (effective_rates_old - self.effective_rates).abs().max(),
                                   (obs_cov_old - self.obs_cov).abs().max(),
                                   (obs_mat_old - self.obs_mat).abs().max())).max()
            logging.info('EM algorithm maximum absolute change {}'.format(eps_max))

            # Check for convergence
            if eps_max < abs_tol_em:
                # Do one last fit with new params
                nat_params_smooth, nat_params_filt, sites = self.infer_counts(t_span, obs_times, obs_counts, options=options)
                break

        # Store output
        if optimize_init_dist_param:
            params['init_dist_param'] = self.init_dist_param.detach().clone()
        if optimize_rates.any():
            params['rates'] = self.rates.detach().clone()
        if optimize_obs_cov:
            params['obs_cov'] = self.obs_cov.detach().clone()
        if optimize_obs_mat:
            params['obs_mat'] = self.obs_mat.detach().clone()

        return nat_params_smooth, nat_params_filt, params


class CRNTrajectory:
    def __init__(self, list_of_events):
        """
        Object for storing a latent CRN Trajectory with callback method for returning counts for given time points
        :param list_of_events: list of discrete events
        """
        self.list_of_events = list_of_events
        self.count_fnct = interp1d(torch.stack([event.time for event in list_of_events]),
                                   torch.stack([event.count for event in list_of_events]), axis=0, kind='previous')
        self.times = torch.tensor(self.count_fnct.x, dtype=self.count_fnct.torch_dtype,
                                  device=self.count_fnct.torch_device)
        self.counts = torch.tensor(self.count_fnct.y.T, dtype=self.count_fnct.torch_dtype,
                                   device=self.count_fnct.torch_device).T

    def __call__(self, *args, **kwargs):
        return self.count_fnct(*args, **kwargs)