import torch
from packages.model import LatentCRN
import logging
from utils.figure_configuration_neurips import figure_configuration_neurips
from utils.utils import set_rng_seed

set_rng_seed(0)

logging.basicConfig(level=logging.INFO)

subs_stoch = torch.tensor([[1, 0, 0, 0],
                           [0, 1, 0, 0],
                           [0, 0, 1, 0],
                           [0, 0, 0, 1]
                           ]).T

prod_stoch = torch.tensor([[0, 1, 0, 0],
                           [0, 0, 1, 0],
                           [0, 0, 0, 1],
                           [1, 0, 0, 0]
                           ]).T

rates = torch.tensor([5e-2, 5e-2, 5e-2, 5e-2])
obs_mat = torch.tensor([[0., 0., 0., 1.]])
obs_cov = torch.tensor([[1. ** 2]])
initial_counts = torch.tensor([20, 20, 20, 20])
init_dist_param = torch.tensor([20., 20., 20., 20.]).log()

crn_model = LatentCRN(subs_stoch, prod_stoch, rates, obs_mat, obs_cov, init_dist_param)
t_span = torch.tensor([0., 100.])
obs_times, _ = torch.sort(t_span[0] + torch.rand(50) * t_span[-1])
latent_trajectory, obs_times, obs_counts = crn_model.simulate(initial_counts, t_span, obs_times)

initial_params = dict(rates=torch.tensor([1e-1, 1e-1, 1e-1, 1e-1])) #Initial guess

nat_params_smooth, nat_params_filt, params = crn_model.fit_params(t_span, obs_times, obs_counts, initial_params,
                                                                  options=dict(abs_tol_em=1e-6))

#Visualize results
import matplotlib
import matplotlib.pyplot as plt


t_grid=torch.linspace(*t_span,1000)
nat_params_smoother_grid=nat_params_smooth(t_grid)
phase=torch.meshgrid(t_grid,torch.arange(10,30).to(dtype=torch.float))
log_score=[]
for t, nat_params_smoother, count in zip(t_grid, nat_params_smoother_grid.T, phase[1]):
    log_score_t=torch.distributions.Poisson(nat_params_smoother.exp()).log_prob(count[...,None])
    log_score.append(log_score_t)
log_score = torch.stack(log_score)

c_light = matplotlib.colors.colorConverter.to_rgba('white', alpha = 0)
handles=[]
colors = ['#d62728','#2ca02c', '#ff7f0e',  '#1f77b4']
vmax=[.15,.15,.15,.15]
vmin=[.01,.01,.01,.01]
figure_configuration_neurips(k_width_height=2., columnwidth_cm=4.67)
for i in range(4):
    plt.figure()
    c_dark= matplotlib.colors.colorConverter.to_rgba(colors[i], alpha = 1)
    cmap_rb = matplotlib.colors.LinearSegmentedColormap.from_list('rb_cmap', [c_light, c_dark], 512)
    plt.pcolor(*phase,  log_score.T[i].exp().T, antialiaseds=True, rasterized=True,cmap=cmap_rb,norm=matplotlib.colors.Normalize(vmax=vmax[i],vmin=vmin[i]))
    handle_gt, = plt.step(latent_trajectory.times, latent_trajectory.counts[...,i], linestyle='dashed', color=colors[i])
    handle_mean, = plt.plot(t_grid, nat_params_smooth(t_grid)[i].exp(), color=colors[i],label='$\mathsf X_{}$'.format(i))
    if i == 3:
        plt.scatter(obs_times, obs_counts, marker='x', color=colors[i])
    plt.ylabel('$X_{}(t)$'.format(i+1))
    plt.tight_layout(pad=0)
    handles.append(handle_mean)

    plt.savefig('loop_{}'.format(i) + '.pdf', format='pdf')
    plt.show()

for i, h in enumerate(handles):
    plt.figure()
    plt.figlegend(handles=[h], frameon=False)
    plt.savefig('loop_' + 'legend_' + str(i+1) + '.pdf', format='pdf')
    plt.show()